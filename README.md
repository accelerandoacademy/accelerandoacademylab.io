This repo holds https://accelerandoacademy.gitlab.io/ and infrastructure for building and hosting it.

This site will be up at https://accelerando.academy/ for executives, developers, parents, children, potential clients, and anyone who wants to understand what we do and our philosophy.

There will be a page for each service we offer:
 - Technology acceleration for CEO/CTOs and startups (Writing code, product design)
 - Development acceleration for teams and companies (Training developers, process improvement)
 - Career acceleration for developers and career transitions (Consultation, coaching)
 - Education acceleration for students and ambitious learners (Tech mentorship, hands-on troubleshooting)
 - Vision acceleration for entrepreneurs and committed dreamers (Lifecoaching, imagineering)


To add: a specific landing page for parents ?

Priority: Landing page for parents.
