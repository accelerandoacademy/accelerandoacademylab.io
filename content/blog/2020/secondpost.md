---
title: "Second Post"
description: "The second post written in Markdown"
date: 2020-09-16
---

Hello internet friends!

Looking at my site now, I have a few things that are obvious changes to make.

1) I need to use an SSH key to push my code, and I need to sign my commits. (This bleeds into 'I need a hardware secure second factor for my key regime' and a lot of other encryption and security to-do items)
2) I need to change the file structure and scripts so my writing content is separated from code,
   and the file organization for my writing content can do away with numbered date folders,
   and just use the available `date` data.
3) I need to make the site look better with some simple CSS. I was planning on trying out Tailwinds CSS for this project,
and there's a Gridsome plugin for it, so that's what's up for now.

We make some changes, and we commit them to see if the site a) stays up lol and b) seamlessly adds this article.

---

It didn't all work - a missing character in the config. Try again.

---

Looking good after switching up my approach, and learning a bit more about gitlab-ci and pages. (Use /public for best results)

Now to remove some of these filestructures.

It works correctly when using `gridsome develop` locally, so let's push it.

---

So, we cleaned up the filestructures. Success, yay.
Let's look at code changes for the site now, including adding css.

We take our instructions from https://gridsome.org/plugins/gridsome-plugin-tailwindcss
We run `npm install -D gridsome-plugin-tailwindcss` and we see how deep this rabbit hole goes.
Then we `./node_modules/.bin/tailwind init`

I change up some configs, run gridsome develop, and I'm looking at a CSS'd-up site.
Firefox is telling me I have sourcemap errors. I guess I'll have to find out why.

The errors don't really go away on the live website either.
It's not clear where the errors are coming from - they might be from my own inbrowser dev tooling.
In fact, the errors are not present on the live site loaded in an incognito de-tooled browser, so that answers that.
We move on - let's STYLE!
:)

---

It's tomorrow, and we've made a logo that will work as a placeholder for a while. We oughtn't spend too too much time on a logo anyways, it's not really what's important.
LibreOffice Draw with inspiration from the Noun Project and away we go!

With a bit of GIMP magic too we have a favicon as well.
Logo and favicon are both betroubled with issues but we'll consider them placeholders until further notice.
They lack colour, rythm, balance, etc. but all can be remedied - at least now I'm not using the logos for gridsome!

---

After taking our calls today (project management stakeholder calls, client sessions, Startupschool founders meeting, calls calls calls!)
We're back for a bit more website work.

Realizing that we've accumulated a lot of tabs open in our browser and our terminal,
we realize that each tab represents a task
and that
DADOY!
We forgot to track 'em!
Time to pull out our kanban board!

Let's see, does gitlab have something servicable?
Yup, just about!
We find "Issue Boards" do what we need - track our tasks
So we can close our tabs and focus on our priorities.

We make some issues to-do.

We find another fascinating feature we'd like to use - perhaps for our parent surveys? But no, because this project is public.
We'll leave service desk off for now.

Our keyword queries bring us to this page, https://dev.to/connorrreilly/building-a-website-blog-with-gridsome-tailwindcss-and-contentful-pt-2-of-2-3p2i
and once we find the code we think we want, we copy the html and styles from the supplied ContentfulBlogPost.vue
We fix the errors that result, and in the end we have a rather handsome blog page!
Much improved!

Let's commit our progress!

We come back to add a picture, since we know things will change over time....
and we have a card for adding a picture.
Really, every post should have more than just a single picture.

[![](/blog-appearance-2020-09-17.png)](/blog-appearance-2020-09-17.png)

Lol to make it work we need a plugin
`npm install @gridsome/transformer-remark`
config changes....
`npm install remark-images`
config changes....

gulp, are we just messing things up?

It seems like we actually need to put our images in gridsome's `static` folder 
to get this to work
for now
hmm


Now that I see the picture, I know it needs a 'before' picture for it to make sense.


But actually, it's been a super long day, so it's time to turn the computer off.
