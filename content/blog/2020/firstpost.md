---
title: "First Post"
description: "The first post written in Markdown"
date: 2020-09-15
---

# Setting up the Accelerando Academy website

An opinionated guide to the tools I reach for first.

The first step for this project is setting up a Gitlab account for Accelerando Academy.
I sign up using my @hey.com email address, and in minutes I'm the proud owner of the accelerandoacademy account on gitlab.com
I create a repo for accelerandoacademy.gitlab.io to allow me to use Gitlab's free static site hosting, and use a .gitlab-ci.yml file to activate the gitlab CI/CD that pushes my static site live whenever I update the repo.
Now that we've got our placeholder site statically hosted at https://accelerandoacademy.gitlab.io , we want to link our real URL accelerando.academy.
We activate the new domain through gitlab's 'pages' settings, and we enable automatic Let's Encrypt certificates, to get that reassuring lock icon.
To complete this setup, we need to log into our registrar and add a CNAME and TXT record to our domain's DNS records.
We log into our registrar, using our 2FA of course, and we add the records. (we're using namecheap as the registrar because of historic intertia, not because of any ideological loyalty. Would be interested in hearing of any more pure organization offering better domain registrar services.)

We activate auto-renewal on our domain registration and whoisgaurd while we're managing our domain.
Of course, nothing ever works the first time, and we need to check some docs, but we get it set up pretty quickly.
accelerando.academy points to our static site on accelerandoacademy.gitlab.io on the first try, but getting the domain verified with a TXT record takes a couple tries.
We use the 'dig' command in our bash terminal to verify our setup.
We verify that our CNAME record is correct, and our TXT records for verification just don't seem to be working.
We check docs, we check configs, we check successful configs from other projects and domains, we create three different experimental TXT records to see if we can get anything to show up in dig for TXT... It doesn't seem to be working. This is always how it is - we are missing some small config fact. Once we find it, we'll be able to move on.

Most of programming and tech is just like this - you need to calmly address the issue at hand over and over until it is solved. It's not personal, it's not emotional, it's just a configuration.
We'll get it soon enough and be able to move to the next step.
If you feel yourself getting frustrated, have a breath and a drink of water, and remind yourself this is step like two out of a thousand. Tech is often a marathon, not a sprint.
Puzzlingly, "dig www.accelerando.academy TXT" works but not the others, the correct ones we actually need. A sense of humour about such things is essential to maintaining our calm patience.
We wait, and eventually the DNS records propogate, and verification succeeds.
Honestly, we're not sure exactly why, but since we wanted it to work and it did, we can move on. :)

Before closing out our namecheap dns dashboard, I enable DNSSEC because the checkbox is right there. We'll see what curious problems that causes for me in the future! :)

Our browser gives us an insecure certificate warning, "Firefox does not trust this site because it uses a certificate that is not valid for accelerando.academy. The certificate is only valid for the following names: *.gitlab.io, gitlab.io"

So, puzzling. Can we fix this easily? It may just be a matter of time: "GitLab is obtaining a Let's Encrypt SSL certificate for this domain. This process can take some time. Please try again later."

Let's check back on that later.

We add a catch-all email redirect - we'll give this domain's email routing more love later.

We did just have to wait! Our little security lock is ON!

Experimental setup
------------------

We're using Grisdome for the first time on this site, so we check the gridsome docs to figure out the setup. https://gridsome.org/docs/
We're going to set up our local dev machine CLI setup, then once we understand how it works we're going to get our .gitlab-ci.yml config set up to activate gridsome's build in a VM on repo push.

we pop a shell in our projects folder and install gridsome/cli, then `gridsome create accelerandoacademy` and... errors! Always errors when trying something new. No surprise there. `Error: Failed to install dependencies with npm.`
We cd into accelerandoacademy folder and `npm install`, perhaps we'll get better results?
I'm guessing this is a yarn/npm incompatibility, and these devs are all on yarn, without testing npm correctly. Yarn, by the way, is facebook jank, and distasteful to use for that reason.
Other issue writers on github also have cli install issues. That's experimental pre-1.0 software for ya!
We try... updating npm, using npm. Not it.
The errors seem to say we need a more current Node, too.
```
npm ERR! Your cache folder contains root-owned files, due to a bug in
npm ERR! previous versions of npm which has since been addressed.
npm ERR!
npm ERR! To permanently fix this problem, please run:
npm ERR!   sudo chown -R 65534:1000 "/home/science/.npm"
```
Okay, we chase this one down. We restinall npm, we reinstall node, we change our permissions.... We remain calm.
We know.
`npm WARN using --force I sure hope you know what you are doing.`
We persevere, and decide to install n, the node version manager, to get around this jank.
`sudo n latest` and... will gridsome work now?
After restarting our terminal, and re-fixing our permissions for npm,
inside our folder, `npm install` finally works without erroring.
One more step down! Now what does it DO?!
we activate `gridsome develop` and check the dev server in the browser.
The defaults are clean, though we get some errors in the terminal. Perhaps they don't really matter.

We make a mental note that we'll need the latest node when we set up our .gitlab-ci.yml

We want to see the results of 'gridsome build' and we get... an error for our favicon.

We load dist/index.html into our browser, but it errors on its own js. Let's try it in a simpleserver. I think it's npx simpleserver? ctrl+r npx shows npx live-server, so we try that.
Image loading errors now. I guess we should expect a lot of problems with gridsome for a while.

We pop a tab for the vue browser extension tools to install later. Let's not get that fancy yet.

So, we have some tech bits more or less. What we're missing is a coherent vision or design of what we want to achieve.

For that, we start with pen and paper.
We also open our accelerandoacademy folder in Atom.

Puzzlingly, src/favicon.png isn't making it into /dist
from `gridsome build`
```
Processing images (9 images) - 0%Error: Failed to process image src/favicon.png.
    at pMap.concurrency (/home/science/projects/accelerandoacademy/node_modules/gridsome/lib/workers/image-processor.js:140:13)
    at processTicksAndRejections (internal/process/task_queues.js:93:5)
```

hmmm... we.... update npm again?
npm rebuild?
lol ok
npm rebuild was the ticket, now gridsome build is processing images correctly.
Jeeze this has been an errorfest! What a bughunt, and we're still just getting set up!
The on-page image loads, but the favicons are still MIA.
Mmmmm clearing browser caches a few times made the favicons appear.
At least we've got that going for us now.

We have gridsome develop running, and we duplicate About.vue into Test.vue to see how the livereload goes.
Feels nice enough. And the loading when clicking around is snappy, which is what using gridsome is all about.

Let's see if we can get gridsome build to static host on gitlab-ci and work with accelerando.academy, with this .md file on /blog.
I expect this to be errorful.
Perhaps only ten or twenty errors before victory.

We port a bunch of gridsome jank and change our .yml, and we push to find out... will it blend?
We read the logs, and it looks successful, but we made a config mistake with our cache of node_modules.
We also made a mistake copying /dist/ into /public/ instead of just dist's contents. Fix that too, try again.

Cool! https://accelerando.academy successfully shows my gridsome dist as public. Looking good!
With out tech infra functional, it's time to actually.... make a website!

We return to our paper and pen designs, and we're going to write code until the site matches the designs.

We go back to our code. To set up our blog from markdown files, we need to get our hands dirty with gridsome plugins.
We get source-filesystem, then configure. We get transformer-remark, then configure. Errors are sprinkled in, we pass over them without comment.

We're having some trouble getting our simple blog compiled, so we check the examples for the simplest implementation: https://github.com/alexbrown/gridsome-starter-minimal-blog

Jeeze, truly nothing is ever easy.

We're going to setup a new project with this template to see how it works.

The example /does not work/. So, that's good to know.
Gridsome is EARLY, very early.
I am ready for more psychic pain, but I'm ready to say out loud no one should use this unless they're a pro with a day to waste learning.
We update the examples dependencies, and it shows us some interesting deprecated jank.

Okay, after more jank, errors, and confusion, I have determined that

typename: CapsCase and CapsCase.vue are references by $page.snakeCase.properties - is that a Vue gotcha I would have known about?
and .md 'title' is autoconverted in slug-format-like-this
so "title: "First Post"" is served at blog/2020/09/15/first-post

Okay, after reading a lot of examples, we finally have a working /blog/ that lists posts with links (poorly, unbeautifully, but improvably)
Let's... fix it linking to "http://localhost:8080/blog/Invalid%20date/Invalid%20date/Invalid%20date/title/"
....
It's unclear if I... changed something? Or if it just started working?
I haven't been committing enough!
Let's commit and push and see if anything works online.

It works! It all seems to work!

Rather than push our luck, we're going to walk away from this while it's still working. :)

We'll be back to make things look nice and implement our real site structure and design next time. :)

Note for next time - talk about the graphql explorer and how it helped you get this up and running today.
